use std::process::Command;
use reqwest::Error;
use std::fs;

extern crate json;
extern crate reqwest;

#[derive(Copy, Clone)]
struct Coords
{
    lat: f64,
    lon: f64
}

struct Direction
{
    distance: f64,
    time: String,
    speed: i32,
    asc: f64,
    des: f64
}

#[tokio::main]
async fn main()
{
    // Ask the user where to start
    let mut start_str = String::new();
    println!("Please enter the address of your start point (\"Address City\" e.g. Neuer Platz 1 Klagenfurt):");
    std::io::stdin().read_line(&mut start_str).expect("error: unable to read user input");

    // Ask the user where to end
    let mut end_str = String::new();
    println!("Please enter the address of your end point (\"Address City\" e.g. Neuer Platz 1 Klagenfurt):");
    std::io::stdin().read_line(&mut end_str).expect("error: unable to read user input");
 
    println!("");

    // Convert start and end to coords
    let start = name_to_coords(start_str).await.unwrap();
    let end = name_to_coords(end_str).await.unwrap();

    // Generate the directions for speeds from 80-130
    let step = 10;
    let mut directions = Vec::new();

    for speed in (80..=130).step_by(step)
    {
        directions.push(get_directions(start, end, speed));
    }

    // General details about the route (distance, asc, des)
    println!("Distance: \t{:3.2}km\nAscent: \t{:6.0}m\nDescent: \t{:6.0}m\n", directions[0].distance, directions[0].asc, directions[0].des);

    // List the duration for the given speed
    for direction in directions
    {
        println!("Speed: {:3}km/h -> Time: {}", direction.speed, direction.time);
    }

    println!("\nPlease check if the following route is correct before relying on the data:");
    println!("https://www.openstreetmap.org/directions?engine=fossgis_osrm_car&route={}%2C{}%3B{}%2C{}", start.lat, start.lon, end.lat, end.lon);
}

// API: https://openrouteservice.org/dev/#/api-docs/v2
fn get_directions(start: Coords, end: Coords, max_speed: i32) -> Direction
{
    let key = read_file("rsc/openrouteservice.txt".to_owned()).replace("\n", "");

    if key == "<PUT YOUR KEY HERE>"
    {
        println!("Please enter you openrouteservice api key to the 'rsc/openrouteservice.txt' file.");
        println!("You can create a key at this url: https://openrouteservice.org/dev/#/home");
        std::process::exit(1);
    }

    let directions_command = Command::new("sh").arg("-c")
        .arg(format!("curl -X POST 'https://api.openrouteservice.org/v2/directions/driving-car' \
        -H 'Content-Type: application/json; charset=utf-8' \
        -H 'Accept: application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8' \
        -H 'Authorization: {key}' \
        -d '{{\"coordinates\":[[{:.6},{:.6}],[{:.6},{:.6}]], \
             \"elevation\":\"true\",\"instructions\":\"false\",\"maneuvers\":\"false\", \
             \"preference\":\"fastest\",\"suppress_warnings\":\"true\",\"units\":\"km\",\"geometry\":\"true\", \
             \"maximum_speed\":{}}}'", start.lon, start.lat, end.lon, end.lat, max_speed))
        .output()
        .expect("https://api.openrouteservice.org/v2/directions/driving-car POST failed.");
    
    let response = json::parse(&String::from_utf8_lossy(&directions_command.stdout)).unwrap();
    let ref summary = &response["routes"][0]["summary"];

    let duration = summary["duration"].as_f64().unwrap() as i32;
    let time = sec_to_time(duration);
    
    return Direction
    {
        distance: summary["distance"].as_f64().unwrap(),
        time: time,
        speed: max_speed,
        asc: summary["ascent"].as_f64().unwrap(),
        des: summary["descent"].as_f64().unwrap()
    };
}

// API: https://opencagedata.com/api
async fn name_to_coords(name: String) -> Result<Coords, Error>
{
    let key = read_file("rsc/opencagedata.txt".to_owned()).replace("\n", "");

    if key == "<PUT YOUR KEY HERE>"
    {
        println!("Please enter you opencagedata api key to the 'rsc/opencagedata.txt' file.");
        println!("You can creat a key at this url: https://opencagedata.com/ap");
        std::process::exit(1);
    }

    let location = name.replace(" ", "-");
    let request = format!("https://api.opencagedata.com/geocode/v1/json?q={location}&key={key}");
    let response = json::parse(&reqwest::get(&request).await?.text().await?).unwrap();
    let osm_url = response["results"][0]["annotations"]["OSM"]["url"].as_str().unwrap();

    let mut lat = osm_url.split("=").collect::<Vec<&str>>()[1].to_owned();
    lat.truncate(8);

    let mut lon = osm_url.split("=").collect::<Vec<&str>>()[2].to_owned();
    lon.truncate(8);

    if lat.chars().last().unwrap() == '&' { lat.truncate(7); }
    if lon.chars().last().unwrap() == '#' { lon.truncate(7); }

    let coord = Coords
    {
        lat: lat.parse::<f64>().unwrap(),
        lon: lon.parse::<f64>().unwrap()
    };

    Ok(coord)
}

fn sec_to_time(duration: i32) -> String
{
    let mut time = String::new().to_owned();

    let days = (duration / (3600 * 24)) % 7;
    let hours = (duration / 3600) % 24;
    let minutes = (duration / 60) % 60;
    let seconds = duration % 60;

    if days > 0 { time.push_str(&format!("{}d ", days)); }
    if hours > 0 { time.push_str(&format!("{}h ", hours)); }
    if minutes > 0 { time.push_str(&format!("{}min ", minutes)); }
    if seconds > 0 { time.push_str(&format!("{}s ", seconds)); }

    time.truncate(time.len() - 1);

    return time;
}

fn read_file(path: String) -> String
{
    return fs::read_to_string(path).expect("Should have been able to read the file");
}

