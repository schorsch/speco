# speco

Comparing the time differences of different speed limits on a given route

# Usage

## Prerequisites

Before being able to run the program you must enter api keys to both files in the "rsc/" folder. You can generate the appropriate keys at the following urls:

- https://openrouteservice.org/dev/#/home
- https://opencagedata.com/api

# Running

To run the program simply cd into the project folder and run `cargo run`.
The program will then ask your for two addresses, a start and an end point in the format <Address> <City> (e.g. "Neuer Platz 1 Klagenfurt" or "102 Lillie Road London").
After providing those addresses the program will provide you with some general information about the route and the time needed if going at a max of 80-130km/h.

Example output:

```
Please enter the address of your start point ("Address City" e.g. Neuer Platz 1 Klagenfurt):
Neuer Platz 1 Klagenfurt
Please enter the address of your end point ("Address City" e.g. Neuer Platz 1 Klagenfurt):
Stephansplatz 1 Wien

Distance: 	300.47km
Ascent: 	  3292m
Descent: 	  3552m

Speed:  80km/h -> Time: 4h 21min 5s
Speed:  90km/h -> Time: 3h 56min 59s
Speed: 100km/h -> Time: 3h 41min 2s
Speed: 110km/h -> Time: 3h 28min 2s
Speed: 120km/h -> Time: 3h 22min 23s
Speed: 130km/h -> Time: 3h 22min 23s

Please check if the following route is correct before relying on the data:
https://www.openstreetmap.org/directions?engine=fossgis_osrm_car&route=46.62397%2C14.30678%3B48.20816%2C16.37348
```
